#!/bin/bash
set -e

./gradlew test
./gradlew jacocoTestReport

./build.sh
docker-compose up -d
export INTEGRATION_TEST_URL=http://127.0.0.1:8080/

sleep 10
./gradlew integrationTest
