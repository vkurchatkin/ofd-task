package me.vkurchatkin.agent.service;

import me.vkurchatkin.agent.service.request.Request;
import me.vkurchatkin.agent.service.response.Response;

public interface Service {
    Response dispatch(Request request);
}
