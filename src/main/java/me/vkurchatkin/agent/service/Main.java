package me.vkurchatkin.agent.service;

import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.filter.MetricsFilter;
import io.prometheus.client.hotspot.DefaultExports;
import me.vkurchatkin.agent.service.config.Config;
import me.vkurchatkin.agent.service.config.EnvConfig;
import me.vkurchatkin.agent.service.data.Dao;
import me.vkurchatkin.agent.service.data.DataSourceFactory;
import me.vkurchatkin.agent.service.data.DbDao;
import me.vkurchatkin.agent.service.request.DomDecoder;
import me.vkurchatkin.agent.service.request.RequestDecoder;
import me.vkurchatkin.agent.service.response.DomEncoder;
import me.vkurchatkin.agent.service.response.ResponseEncoder;
import me.vkurchatkin.agent.service.security.MultiHash;
import me.vkurchatkin.agent.service.security.PasswordHash;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.FilterDef;
import org.apache.tomcat.util.descriptor.web.FilterMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.File;
import java.io.IOException;

public class Main {
    private final static String SERVICE_SERVLET = "SERVICE_SERVLET";
    private final static String METRICS_SERVLET = "METRICS_SERVLET";
    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        Config config = new EnvConfig();

        Tomcat tomcat = new Tomcat();
        tomcat.setPort(config.getHttpPort());
        tomcat.getConnector().setProperty("address", config.getHttpHost());

        Context ctx = tomcat.addContext("", new File(".").getAbsolutePath());

        RequestDecoder requestDecoder = new DomDecoder();
        ResponseEncoder responseEncoder = new DomEncoder();
        PasswordHash passwordHash = new MultiHash();
        Dao dao = new DbDao(DataSourceFactory.newDataSource(config));

        logger.info("Connected to database");

        Service service = new ServiceImpl(dao, passwordHash);

        Tomcat.addServlet(ctx, SERVICE_SERVLET, new Servlet(requestDecoder, responseEncoder, service));
        ctx.addServletMappingDecoded("/", SERVICE_SERVLET);

        DefaultExports.initialize();

        Tomcat.addServlet(ctx, METRICS_SERVLET, new MetricsServlet());
        ctx.addServletMappingDecoded("/metrics", METRICS_SERVLET);

        FilterDef filterDef = new FilterDef();
        filterDef.setFilterName("metricsFilter");
        filterDef.setFilter(new MetricsFilter("http_request_duration_seconds", "Http request duration", 0, new double[]{0.005, 0.01, 0.025, 0.05, 0.075, 0.1, 0.25, 0.5, 0.75, 1, 2.5, 5, 7.5, 10}));
        ctx.addFilterDef(filterDef);

        FilterMap filterMap = new FilterMap();
        filterMap.addServletName(SERVICE_SERVLET);
        filterMap.setFilterName("metricsFilter");
        ctx.addFilterMap(filterMap);

        tomcat.start();
        tomcat.getServer().await();
    }
}
