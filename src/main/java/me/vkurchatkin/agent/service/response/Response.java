package me.vkurchatkin.agent.service.response;

import java.util.Map;

public interface Response {
    Code getCode();
    Map<String, String> getExtra();
}
