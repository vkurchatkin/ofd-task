package me.vkurchatkin.agent.service.response;

import me.vkurchatkin.agent.service.util.ThreadLocalDocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Map;

public class DomEncoder implements ResponseEncoder {
    private final ThreadLocalDocumentBuilder documentBuilder = new ThreadLocalDocumentBuilder();
    @Override
    public String encode(Response response) {

        Document doc = documentBuilder.get().newDocument();
        doc.setXmlStandalone(true);
        Element root = doc.createElement("response");
        doc.appendChild(root);

        Element resultCodeEl = doc.createElement("result-code");
        resultCodeEl.setTextContent(String.valueOf(response.getCode().getIntCode()));
        root.appendChild(resultCodeEl);

        Map<String, String> extra = response.getExtra();

        for (String key: extra.keySet()) {
            String value = extra.get(key);
            Element el = doc.createElement("extra");
            el.setAttribute("name", key);
            el.setTextContent(value);
            root.appendChild(el);
        }

        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            return writer.getBuffer().toString();
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

}
