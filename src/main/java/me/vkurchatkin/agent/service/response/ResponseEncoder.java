package me.vkurchatkin.agent.service.response;

public interface ResponseEncoder {
    String encode(Response response);
}
