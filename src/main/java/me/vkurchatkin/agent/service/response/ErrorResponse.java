package me.vkurchatkin.agent.service.response;

import java.util.HashMap;
import java.util.Map;

public class ErrorResponse implements Response {
    private final Code code;

    public ErrorResponse(Code code) {
        this.code = code;
    }

    @Override
    public Code getCode() {
        return code;
    }

    @Override
    public Map<String, String> getExtra() {
        return new HashMap<>();
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "code=" + code +
                '}';
    }
}
