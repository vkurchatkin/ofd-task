package me.vkurchatkin.agent.service.response;

import java.util.HashMap;
import java.util.Map;

public class CreateAgentResponse implements Response {
    @Override
    public Code getCode() {
        return Code.OK;
    }

    @Override
    public Map<String, String> getExtra() {
        return new HashMap<>();
    }

    @Override
    public String toString() {
        return "CreateAgentResponse{}";
    }
}
