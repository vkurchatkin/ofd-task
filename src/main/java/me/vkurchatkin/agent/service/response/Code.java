package me.vkurchatkin.agent.service.response;

public enum Code {
    OK(0),
    LOGIN_EXISTS(1),
    ERROR(2),
    INVALID_LOGIN(3),
    INVALID_PASSWORD(4);

    private final int code;

    Code(int code) {
        this.code = code;
    }

    public int getIntCode() {
        return code;
    }
}
