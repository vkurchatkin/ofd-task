package me.vkurchatkin.agent.service.response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class GetBalanceResponse implements Response {
    private final BigDecimal balance;

    public GetBalanceResponse(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public Code getCode() {
        return Code.OK;
    }

    @Override
    public Map<String, String> getExtra() {
        Map<String, String> extra = new HashMap<>();
        extra.put("balance", String.valueOf(balance));
        return extra;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "GetBalanceResponse{" +
                "balance=" + balance +
                '}';
    }
}
