package me.vkurchatkin.agent.service;

import me.vkurchatkin.agent.service.request.RequestDecoder;
import me.vkurchatkin.agent.service.request.DecoderException;
import me.vkurchatkin.agent.service.request.Request;
import me.vkurchatkin.agent.service.response.Code;
import me.vkurchatkin.agent.service.response.ErrorResponse;
import me.vkurchatkin.agent.service.response.Response;
import me.vkurchatkin.agent.service.response.ResponseEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Servlet extends HttpServlet {
    private final RequestDecoder requestDecoder;
    private final ResponseEncoder responseEncoder;
    private final Service service;
    private final Logger logger = LoggerFactory.getLogger(Servlet.class);

    public Servlet(RequestDecoder requestDecoder, ResponseEncoder responseEncoder, Service service) {
        this.requestDecoder = requestDecoder;
        this.responseEncoder = responseEncoder;
        this.service = service;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String contentType = req.getContentType();

        if (contentType != null && !contentType.contains("xml")) {
            resp.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            return;
        }

        Request request;

        try {
            request = requestDecoder.decode(req.getInputStream());
        } catch (DecoderException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        Response response;

        try {
            response = service.dispatch(request);
        } catch (Exception e) {
            logger.error("Failed to dispatch request", e);
            response = new ErrorResponse(Code.ERROR);
        }

        byte[] body = responseEncoder.encode(response).getBytes(StandardCharsets.UTF_8);
        resp.setContentLength(body.length);
        resp.setContentType("application/xml; charset=utf-8");
        resp.getOutputStream().write(body);
    }
}
