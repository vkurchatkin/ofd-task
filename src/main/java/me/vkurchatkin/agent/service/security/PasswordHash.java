package me.vkurchatkin.agent.service.security;

public interface PasswordHash {
    String createHash(String input);
    boolean verifyHash(String input, String hash);
}
