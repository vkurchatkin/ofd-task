package me.vkurchatkin.agent.service.security;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class PBKDF2Hash implements PasswordHash {
    private final static int KEY_LENGTH = 256;
    private final static int SALT_LENGTH = 256;
    private final static int ITERATIONS = 10000;
    private final static String ALGORITHM = "PBKDF2WithHmacSHA512";

    private final SecureRandom random = new SecureRandom();

    @Override
    public String createHash(String input) {
        byte[] salt = generateSalt();
        byte[] hash = pbkdf2(input.toCharArray(), salt, ITERATIONS, KEY_LENGTH);
        return ITERATIONS + ":" + toHexString(salt) + ":" + toHexString(hash);
    }

    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int keyLength) {
        try {
            PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
            SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean secureEquals(byte[] a, byte[] b) {
        int diff = a.length ^ b.length;
        for(int i = 0; i < a.length && i < b.length; i++)
            diff |= a[i] ^ b[i];
        return diff == 0;
    }

    @Override
    public boolean verifyHash(String input, String hash) {
        String[] parts = hash.split(":");

        if (parts.length != 3) {
            return false;
        }

        try {
            int iterations = Integer.parseInt(parts[0]);
            byte[] salt = fromHexString(parts[1]);
            byte[] hashBytes = fromHexString(parts[2]);
            byte[] testHash = pbkdf2(input.toCharArray(), salt, iterations, hashBytes.length * 8);

            return secureEquals(hashBytes, testHash);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private byte[] generateSalt() {
        byte[] salt = new byte[SALT_LENGTH / 8];
        random.nextBytes(salt);
        return salt;
    }

    private String toHexString(byte[] data) {
        StringBuilder builder = new StringBuilder();
        for(byte b : data) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    private static byte[] fromHexString(String hex) {
        byte[] binary = new byte[hex.length() / 2];
        for(int i = 0; i < binary.length; i++) {
            binary[i] = (byte)Integer.parseInt(hex.substring(i * 2, i * 2 + 2), 16);
        }
        return binary;
    }

}
