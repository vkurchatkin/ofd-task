package me.vkurchatkin.agent.service.security;

import java.util.HashMap;
import java.util.Map;

public class MultiHash implements PasswordHash {
    private final Map<String, PasswordHash> allImpls = new HashMap<>();
    private final static String PREFERRED_IMPL = "pbkdf2";

    public MultiHash() {
        allImpls.put("pbkdf2", new PBKDF2Hash());
    }

    public void addHash(String key, PasswordHash hash) {
        allImpls.put(key, hash);
    }

    @Override
    public String createHash(String input) {
        PasswordHash impl = allImpls.get(PREFERRED_IMPL);
        return PREFERRED_IMPL + ":" + impl.createHash(input);
    }

    @Override
    public boolean verifyHash(String input, String hash) {
        int idx = hash.indexOf(':');

        if (idx == -1) {
            return false;
        }

        String impKey = hash.substring(0, idx);
        PasswordHash impl = allImpls.get(impKey);

        if (impl == null) {
            return false;
        }

        String restHash = hash.substring(idx + 1);
        return impl.verifyHash(input, restHash);
    }
}
