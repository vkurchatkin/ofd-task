package me.vkurchatkin.agent.service.request;

import java.io.IOException;
import java.io.InputStream;

public interface RequestDecoder {
    Request decode(InputStream stream) throws DecoderException, IOException;
}
