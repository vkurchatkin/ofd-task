package me.vkurchatkin.agent.service.request;

public enum RequestType {
    CREATE_AGENT,
    GET_BALANCE
}
