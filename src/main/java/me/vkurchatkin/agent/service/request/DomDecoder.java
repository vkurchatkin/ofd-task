package me.vkurchatkin.agent.service.request;

import me.vkurchatkin.agent.service.util.ThreadLocalDocumentBuilder;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import java.io.IOException;
import java.io.InputStream;

public class DomDecoder implements RequestDecoder {

    private final ThreadLocalDocumentBuilder documentBuilder = new ThreadLocalDocumentBuilder();

    @Override
    public Request decode(InputStream stream) throws DecoderException, IOException {
        DocumentBuilder builder = this.documentBuilder.get();
        try {
            Document doc = builder.parse(stream);
            Element root = doc.getDocumentElement();

            if (!root.getTagName().equals("request")) {
                throw new DecoderException();
            }

            NodeList childNodes = root.getChildNodes();

            RequestType requestType = null;
            String login = null;
            String password = null;

            for (int i = 0; i < childNodes.getLength(); i++) {
                Node node = childNodes.item(i);

                if (node.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                Element el = (Element) node;

                if (el.getTagName().equals("request-type")) {
                    requestType = parseRequestType(getNodeText(el));
                } else if (el.getTagName().equals("extra")) {
                    if (el.getAttribute("name").equals("login")) {
                        login = getNodeText(el);
                    } else if (el.getAttribute("name").equals("password")) {
                        password = getNodeText(el);
                    }
                } else {
                    throw new DecoderException();
                }
            }

            if (requestType == null || login == null || password == null) {
                throw new DecoderException();
            }

            return new Request(requestType, login, password);

        } catch (SAXException e) {
            throw new DecoderException(e);
        }
    }

    private RequestType parseRequestType(String rawStr) throws DecoderException {
        switch (rawStr) {
            case "CREATE-AGT": return RequestType.CREATE_AGENT;
            case "GET-BALANCE": return RequestType.GET_BALANCE;
        }

        throw new DecoderException();
    }

    private String getNodeText(Node root) throws DecoderException {
        NodeList nodes = root.getChildNodes();

        if (nodes.getLength() == 0) {
            return "";
        }

        if (nodes.getLength() > 1) {
            throw new DecoderException();
        }

        Node node = nodes.item(0);

        if (node.getNodeType() != Node.TEXT_NODE) {
            throw new DecoderException();
        }

        return node.getTextContent();
    }
}
