package me.vkurchatkin.agent.service.request;

public class Request {
    private final RequestType type;
    private final String login;
    private final String password;

    public Request(RequestType type, String login, String password) {
        this.type = type;
        this.login = login;
        this.password = password;
    }

    public RequestType getType() {
        return type;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Request{" +
                "type=" + type +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
