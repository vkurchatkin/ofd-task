package me.vkurchatkin.agent.service.request;

public class DecoderException extends Exception {
    public DecoderException(String message) {
        super(message);
    }

    public DecoderException(String message, Throwable cause) {
        super(message, cause);
    }

    public DecoderException(Throwable cause) {
        super(cause);
    }

    public DecoderException() {
        super();
    }
}
