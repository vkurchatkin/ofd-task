package me.vkurchatkin.agent.service.data;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class MemoryDao implements Dao {
    private final Map<String, Agent> agentMap = new ConcurrentHashMap<>();
    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void createAgent(String login, String password) throws LoginExistsException {
        Agent existingAgent = findAgentByLogin(login);

        if (existingAgent != null) {
            throw new LoginExistsException();
        }

        int id = counter.incrementAndGet();
        Agent agent = new Agent(id, login, password, new BigDecimal(0));
        agentMap.put(login, agent);
    }

    @Override
    public Agent findAgentByLogin(String login) {
        return agentMap.get(login);
    }
}
