package me.vkurchatkin.agent.service.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbDao implements Dao {
    private final DataSource dataSource;
    private final static String UNIQUE_CONSTRAINT_VIOLATION_CODE = "23505";
    private final Logger logger = LoggerFactory.getLogger(DbDao.class);

    public DbDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final static String INSERT_AGENT_STATEMENT = "INSERT INTO agents (login, password, balance) VALUES (?, ?, ?) ";

    @Override
    public void createAgent(String login, String password) throws LoginExistsException, DaoException {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_AGENT_STATEMENT);
            statement.setString(1, login);
            statement.setString(2, password);
            statement.setBigDecimal(3, new BigDecimal(0));

            try {
                statement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();

                if (e.getSQLState().equals(UNIQUE_CONSTRAINT_VIOLATION_CODE)) {
                    throw new LoginExistsException();
                } else {
                    throw e;
                }
            }

            statement.close();
        } catch (SQLException e) {
            logger.error("Failed to create agent", e);
            throw new DaoException();
        }
    }

    private final static String FIND_AGENT_QUERY = "SELECT * FROM agents WHERE login = ?";

    @Override
    public Agent findAgentByLogin(String login) throws DaoException {
        try (Connection connection = dataSource.getConnection()) {
            // Кэширование  PreparedStatement происходит на уровне драйвера
            // https://github.com/pgjdbc/pgjdbc/pull/319
            PreparedStatement statement = connection.prepareStatement(FIND_AGENT_QUERY);
            statement.setString(1, login);
            ResultSet res = statement.executeQuery();

            Agent agent = null;

            if (res.next()) {
                agent = mapAgent(res);
            }

            res.close();
            statement.close();

            return agent;

        } catch (SQLException e) {
            logger.error("Failed to find agent", e);
            throw new DaoException();
        }
    }

    private static Agent mapAgent(ResultSet res) throws SQLException {
        int id = res.getInt("id");
        String login = res.getString("login");
        String password = res.getString("password");
        BigDecimal balance = res.getBigDecimal("balance");

        return new Agent(id, login, password, balance);
    }
}
