package me.vkurchatkin.agent.service.data;

import java.math.BigDecimal;

public class Agent {
    private final int id;
    private final String login;
    private final String password;
    private final BigDecimal balance;

    public Agent(int id, String login, String password, BigDecimal balance) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", balance=" + balance +
                '}';
    }
}
