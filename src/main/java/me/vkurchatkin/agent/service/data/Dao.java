package me.vkurchatkin.agent.service.data;

public interface Dao {
    void createAgent(String login, String password) throws LoginExistsException, DaoException;
    Agent findAgentByLogin(String login) throws DaoException;
}
