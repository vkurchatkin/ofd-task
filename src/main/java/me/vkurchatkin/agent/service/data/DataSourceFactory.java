package me.vkurchatkin.agent.service.data;

import me.vkurchatkin.agent.service.config.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.Properties;

public class DataSourceFactory {
    public static DataSource newDataSource(Config config) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        hikariConfig.setUsername(config.getDbUsername());
        hikariConfig.setPassword(config.getDbPassword());
        hikariConfig.setInitializationFailTimeout(10000);
        hikariConfig.setAutoCommit(false);

        Properties props = new Properties();

        props.setProperty("databaseName", config.getDbName());
        props.setProperty("portNumber", config.getDbPort());
        props.setProperty("serverName", config.getDbHost());

        hikariConfig.setDataSourceProperties(props);
        return new HikariDataSource(hikariConfig);
    }
}
