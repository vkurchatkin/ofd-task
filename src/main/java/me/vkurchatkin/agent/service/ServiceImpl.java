package me.vkurchatkin.agent.service;

import me.vkurchatkin.agent.service.data.Agent;
import me.vkurchatkin.agent.service.data.Dao;
import me.vkurchatkin.agent.service.data.DaoException;
import me.vkurchatkin.agent.service.data.LoginExistsException;
import me.vkurchatkin.agent.service.request.Request;
import me.vkurchatkin.agent.service.response.*;
import me.vkurchatkin.agent.service.security.PasswordHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceImpl implements Service {
    private final Dao dao;
    private final PasswordHash passwordHash;
    private final Logger logger = LoggerFactory.getLogger(ServiceImpl.class);

    public ServiceImpl(Dao dao, PasswordHash passwordHash) {
        this.dao = dao;
        this.passwordHash = passwordHash;
    }

    @Override
    public Response dispatch(Request request) {
        try {
            switch (request.getType()) {
                case GET_BALANCE:
                    return dispatchGetBalance(request);
                case CREATE_AGENT:
                    return dispatchCreateAgent(request);

                default:
                    return null;
            }
        } catch (DaoException e) {
            logger.error("Database error", e);
            return new ErrorResponse(Code.ERROR);
        }
    }

    private Response dispatchGetBalance(Request request) throws DaoException {
        Agent agent = dao.findAgentByLogin(request.getLogin());

        if (agent == null) {
            return new ErrorResponse(Code.INVALID_LOGIN);
        }

        boolean auth = passwordHash.verifyHash(request.getPassword(), agent.getPassword());

        if (!auth) {
            return new ErrorResponse(Code.INVALID_PASSWORD);
        }

        return new GetBalanceResponse(agent.getBalance());
    }

    private Response dispatchCreateAgent(Request request) throws DaoException {
        Agent existingAgent = dao.findAgentByLogin(request.getLogin());

        if (existingAgent != null) {
            return new ErrorResponse(Code.LOGIN_EXISTS);
        }

        String hashedPassword = passwordHash.createHash(request.getPassword());
        try {
            dao.createAgent(request.getLogin(), hashedPassword);
            return new CreateAgentResponse();
        } catch (LoginExistsException e) {
            return new ErrorResponse(Code.LOGIN_EXISTS);
        }
    }
}
