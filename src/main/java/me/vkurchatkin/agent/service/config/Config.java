package me.vkurchatkin.agent.service.config;

public interface Config {
    String getDbName();
    String getDbPort();
    String getDbHost();
    String getDbUsername();
    String getDbPassword();
    int getHttpPort();
    String getHttpHost();
}
