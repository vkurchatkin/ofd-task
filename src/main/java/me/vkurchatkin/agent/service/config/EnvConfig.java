package me.vkurchatkin.agent.service.config;

public class EnvConfig implements Config {
    private final static String DB_NAME = "DB_NAME";
    private final static String DB_HOST = "DB_HOST";
    private final static String DB_PORT = "DB_PORT";
    private final static String DB_USERNAME = "DB_USERNAME";
    private final static String DB_PASSWORD = "DB_PASSWORD";
    private final static String HTTP_PORT = "HTTP_PORT";
    private final static String HTTP_HOST = "HTTP_HOST";
    private final static String DEFAULT_HTTP_HOST = "0.0.0.0";


    @Override
    public String getDbName() {
        return getString(DB_NAME);
    }

    @Override
    public String getDbPort() {
        return getString(DB_PORT);
    }

    @Override
    public String getDbHost() {
        return getString(DB_HOST);
    }

    @Override
    public String getDbUsername() {
        return getString(DB_USERNAME);
    }

    @Override
    public String getDbPassword() {
        return getString(DB_PASSWORD);
    }

    @Override
    public int getHttpPort() {
        return getInt(HTTP_PORT);
    }

    @Override
    public String getHttpHost() {
        return getString(HTTP_HOST, DEFAULT_HTTP_HOST);
    }

    private String getString(String key, String defaultValue) {
        String value = System.getenv(key);

        if (value == null) {
            if (defaultValue != null) {
                value = defaultValue;
            } else {
                throw new RuntimeException("Failed to read env var: " + key);
            }
        }

        return value;
    }

    private String getString(String key) {
        return getString(key, null);
    }

    private int getInt(String key) {
        return Integer.parseInt(getString(key));
    }
}
