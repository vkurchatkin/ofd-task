package me.vkurchatkin.agent.service.test.integration;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;


public class IntegrationTest {
    private final static String URL = System.getenv("INTEGRATION_TEST_URL");
    private final static HttpClient client = new HttpClient();

    private class Response {
        String code;
        Map<String, String> extra = new HashMap<>();
    }

    private Response parseResponse(byte[] body) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.parse(new ByteArrayInputStream(body));
        Element root = doc.getDocumentElement();

        if (!root.getTagName().equals("response")) {
            throw new RuntimeException();
        }

        NodeList childNodes = root.getChildNodes();

        Response response = new Response();

        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);

            if (node.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            Element el = (Element) node;

            if (el.getTagName().equals("result-code")) {
                response.code = el.getTextContent();
            } else if (el.getTagName().equals("extra")) {
                response.extra.put(el.getAttribute("name"), el.getTextContent());
            }
        }

        return response;
    }

    private Response makeRequest(String body) throws Exception {
        PostMethod method = new PostMethod(URL);
        method.setRequestEntity(new StringRequestEntity(body));
        client.executeMethod(method);
        assertThat(method.getStatusCode(), equalTo(200));
        byte[] responseBody = method.getResponseBody();
        method.releaseConnection();
        return parseResponse(responseBody);
    }

    @Test
    public void testInvalidLogin() throws Exception {
        String login = UUID.randomUUID().toString();
        String password = "qwerty";

        String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <request>\n" +
                "      <request-type>GET-BALANCE</request-type>\n" +
                "      <extra name=\"login\">" + login + "</extra>\n" +
                "      <extra name=\"password\">" + password + "</extra>\n" +
                "    </request>";


        Response response = makeRequest(request);
        assertThat(response.code, equalTo("3"));

    }

    @Test
    public void testCreateAgent() throws Exception {
        String login = UUID.randomUUID().toString();
        String password = "qwerty";

        String request1 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <request>\n" +
                "      <request-type>CREATE-AGT</request-type>\n" +
                "      <extra name=\"login\">" + login + "</extra>\n" +
                "      <extra name=\"password\">" + password + "</extra>\n" +
                "    </request>";


        Response response = makeRequest(request1);
        assertThat(response.code, equalTo("0"));

        String request2 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <request>\n" +
                "      <request-type>GET-BALANCE</request-type>\n" +
                "      <extra name=\"login\">" + login + "</extra>\n" +
                "      <extra name=\"password\">" + password + "</extra>\n" +
                "    </request>";


        Response response2 = makeRequest(request2);
        assertThat(response2.code, equalTo("0"));

        assertThat(response2.extra, hasEntry("balance", "0"));
    }

    @Test
    public void testInvalidPassword() throws Exception {
        String login = UUID.randomUUID().toString();
        String password = "qwerty";

        String request1 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <request>\n" +
                "      <request-type>CREATE-AGT</request-type>\n" +
                "      <extra name=\"login\">" + login + "</extra>\n" +
                "      <extra name=\"password\">" + password + "</extra>\n" +
                "    </request>";


        Response response = makeRequest(request1);
        assertThat(response.code, equalTo("0"));

        String request2 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <request>\n" +
                "      <request-type>GET-BALANCE</request-type>\n" +
                "      <extra name=\"login\">" + login + "</extra>\n" +
                "      <extra name=\"password\">bar</extra>\n" +
                "    </request>";


        Response response2 = makeRequest(request2);
        assertThat(response2.code, equalTo("4"));
    }

    @Test
    public void testCreateAgentDuplicate() throws Exception {
        String login = UUID.randomUUID().toString();
        String password = "qwerty";

        String request = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <request>\n" +
                "      <request-type>CREATE-AGT</request-type>\n" +
                "      <extra name=\"login\">" + login + "</extra>\n" +
                "      <extra name=\"password\">" + password + "</extra>\n" +
                "    </request>";


        makeRequest(request);
        Response response = makeRequest(request);
        assertThat(response.code, equalTo("1"));
    }
}
