package me.vkurchatkin.agent.service.test;

import me.vkurchatkin.agent.service.security.MultiHash;
import me.vkurchatkin.agent.service.security.PasswordHash;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HashTest {
    @Test
    public void testMultihashCreate() {
        PasswordHash hash = new MultiHash();
        String hashed = hash.createHash("foo");

        assertThat("hash is correct", hash.verifyHash("foo", hashed));
        assertThat("hash is not correct", !hash.verifyHash("foo1", hashed));
    }

    @Test
    public void testMultihashLegacy() {
        MultiHash hash = new MultiHash();
        PasswordHash legacyHash = mock(PasswordHash.class);

        when(legacyHash.verifyHash("foo", "bar")).thenReturn(true);

        hash.addHash("legacy", legacyHash);
        assertThat("hash is correct", hash.verifyHash("foo", "legacy:bar"));
    }

    @Test
    public void testMultihashInvalid() {
        MultiHash hash = new MultiHash();

        assertThat("hash is not correct", !hash.verifyHash("foo", "invalid:bar"));
        assertThat("hash is not correct", !hash.verifyHash("foo", "invalid_bar"));
    }
}
