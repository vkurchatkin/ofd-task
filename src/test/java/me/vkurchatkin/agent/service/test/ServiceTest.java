package me.vkurchatkin.agent.service.test;

import me.vkurchatkin.agent.service.Service;
import me.vkurchatkin.agent.service.ServiceImpl;
import me.vkurchatkin.agent.service.data.Agent;
import me.vkurchatkin.agent.service.data.Dao;
import me.vkurchatkin.agent.service.data.DaoException;
import me.vkurchatkin.agent.service.data.LoginExistsException;
import me.vkurchatkin.agent.service.request.Request;
import me.vkurchatkin.agent.service.request.RequestType;
import me.vkurchatkin.agent.service.response.Code;
import me.vkurchatkin.agent.service.response.Response;
import me.vkurchatkin.agent.service.security.PasswordHash;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.Mockito.*;

public class ServiceTest {

    @Test
    public void testCreateAgent() throws LoginExistsException, DaoException {
        Dao dao = mock(Dao.class);
        PasswordHash passwordHash = mock(PasswordHash.class);
        when(passwordHash.createHash(anyString())).thenReturn("hash");

        Service service = new ServiceImpl(dao, passwordHash);
        Request req = new Request(RequestType.CREATE_AGENT, "foo", "bar");
        Response response = service.dispatch(req);

        assertThat(response.getCode(), equalTo(Code.OK));
        assertThat("Response has no extras", response.getExtra().isEmpty());
        verify(dao).createAgent("foo", "hash");
    }

    @Test
    public void testCreateAgentDuplicate() throws DaoException {
        Dao dao = mock(Dao.class);

        when(dao.findAgentByLogin(anyString())).thenReturn(mock(Agent.class));
        PasswordHash passwordHash = mock(PasswordHash.class);

        Service service = new ServiceImpl(dao, passwordHash);
        Request req = new Request(RequestType.CREATE_AGENT, "foo", "bar");
        Response response = service.dispatch(req);

        assertThat(response.getCode(), equalTo(Code.LOGIN_EXISTS));
        assertThat("Response has no extras", response.getExtra().isEmpty());
    }

    @Test
    public void testCreateAgentDuplicateRace() throws Exception {
        Dao dao = mock(Dao.class);

        doThrow(LoginExistsException.class).when(dao).createAgent(anyString(), anyString());
        PasswordHash passwordHash = mock(PasswordHash.class);
        when(passwordHash.createHash(anyString())).thenReturn("hash");

        Service service = new ServiceImpl(dao, passwordHash);
        Request req = new Request(RequestType.CREATE_AGENT, "foo", "bar");
        Response response = service.dispatch(req);

        assertThat(response.getCode(), equalTo(Code.LOGIN_EXISTS));
        assertThat("Response has no extras", response.getExtra().isEmpty());
    }

    @Test
    public void testGetBalance() throws DaoException {
        Dao dao = mock(Dao.class);

        when(dao.findAgentByLogin("foo")).thenReturn(new Agent(1, "foo", "hash", new BigDecimal(42)));

        PasswordHash passwordHash = mock(PasswordHash.class);
        when(passwordHash.verifyHash(any(), any())).thenReturn(true);

        Service service = new ServiceImpl(dao, passwordHash);
        Request req = new Request(RequestType.GET_BALANCE, "foo", "bar");
        Response response = service.dispatch(req);

        assertThat(response.getCode(), equalTo(Code.OK));
        assertThat(response.getExtra(), hasEntry("balance", "42"));
    }

    @Test
    public void testGetBalanceInvalidLogin() throws DaoException {
        Dao dao = mock(Dao.class);

        PasswordHash passwordHash = mock(PasswordHash.class);
        when(passwordHash.verifyHash(any(), any())).thenReturn(true);

        Service service = new ServiceImpl(dao, passwordHash);
        Request req = new Request(RequestType.GET_BALANCE, "foo", "bar");
        Response response = service.dispatch(req);

        assertThat(response.getCode(), equalTo(Code.INVALID_LOGIN));
    }

    @Test
    public void testGetBalanceInvalidPassword() throws DaoException {
        Dao dao = mock(Dao.class);
        when(dao.findAgentByLogin("foo")).thenReturn(new Agent(1, "foo", "hash", new BigDecimal(42)));

        PasswordHash passwordHash = mock(PasswordHash.class);
        when(passwordHash.verifyHash(any(), any())).thenReturn(false);

        Service service = new ServiceImpl(dao, passwordHash);
        Request req = new Request(RequestType.GET_BALANCE, "foo", "bar");
        Response response = service.dispatch(req);

        assertThat(response.getCode(), equalTo(Code.INVALID_PASSWORD));
    }
}
