package me.vkurchatkin.agent.service.test;

import me.vkurchatkin.agent.service.request.*;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;


public class DecoderTest {

    private InputStream createStream(String str) {
        return new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void testDecoder() throws DecoderException, IOException {
        RequestDecoder decoder = new DomDecoder();
        Request req = decoder.decode(createStream("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <request>\n" +
                "      <request-type>GET-BALANCE</request-type>\n" +
                "      <extra name=\"login\">foo</extra>\n" +
                "      <extra name=\"password\">bar</extra>\n" +
                "    </request>"));

        assertThat(req, samePropertyValuesAs(new Request(RequestType.GET_BALANCE, "foo", "bar")));
    }

    @Test(expected = DecoderException.class)
    public void testDecoderInvalid() throws DecoderException, IOException {
        RequestDecoder decoder = new DomDecoder();
        decoder.decode(createStream("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "    <foo/>\n"));
    }

    @Test(expected = DecoderException.class)
    public void testDecoderNotXml() throws DecoderException, IOException {
        RequestDecoder decoder = new DomDecoder();
        decoder.decode(createStream("foo"));
    }
}
