FROM openjdk:8-alpine

COPY ./build/libs/agent-service.jar /app.jar

CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]
