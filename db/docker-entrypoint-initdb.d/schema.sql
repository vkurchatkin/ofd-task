CREATE TABLE agents
(
  id SERIAL PRIMARY KEY,
  login TEXT NOT NULL,
  password TEXT NOT NULL,
  balance NUMERIC NOT NULL
)
WITH (
  OIDS=FALSE
);

CREATE UNIQUE INDEX idx_agents_login ON agents (login);
