
[![CircleCI](https://circleci.com/bb/vkurchatkin/ofd-task.svg?style=svg)](https://circleci.com/bb/vkurchatkin/ofd-task)


# Tasks

 - `gradle shadowJar`
 - `gradle test`
 - `jacocoTestReport`

# Сборка

  - `./build.sh` - сборка единого `.jar` и Docker-контейнера;
  - `gradle test` - тесты;
  - `gradle jacocoTestReport` - генерация отчета о покрытии;

# docker-compose

 - `docker-compose up` - запуск конфигурации;
